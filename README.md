# VEAL
VEAL is a younger leaner take on BeEF. 

VEAL uses Node/Express server-side, with Angular on the client-side.

Hooked browsers' sessions are based around the use of WebSockets instead of requests on an interval.

Run npm install and bower install to get started
